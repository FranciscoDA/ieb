#! /bin/bash

function wait_db {
    DB="$1"
    [ -z "$DB" ] && DB='default'
    for i in {1..5} ; do
        # test the db connection by opening a sql shell
        set +e  # disable exit on error
        echo '' | python manage.py dbshell
        exit_code="$?"
        set -e  # reenable exit on error

        if [ "$exit_code" == "2" ] ; then
            echo 'Could not connect to database, retrying...'
            sleep 3
        else
            break
        fi
    done
}


wait_db default
python manage.py migrate
case "$1" in
    runserver)
        python manage.py runserver 0.0.0.0:8000
        ;;
    *) exec "$@"
esac