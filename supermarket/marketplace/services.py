from decimal import Decimal as D
import statistics
import random
from asgiref.sync import async_to_sync

from .models import Product
from channels.layers import get_channel_layer
from .serializers import ProductSerializerWithCategory


class PriceUpdaterService:
    def update_all_product_prices(self):
        counter = 0
        for product in Product.objects.all():
            mean_price = statistics.mean([product.buy_price, product.sell_retail_price, product.sell_wholesale_price])
            price_stdev = statistics.stdev([product.buy_price, product.sell_retail_price, product.sell_wholesale_price])

            new_midprice = random.normalvariate(mu=float(mean_price), sigma=float(price_stdev))
            new_midprice = D(new_midprice).quantize(D(10) ** -9)
            self.update_product_price(
                product,
                product.buy_price / mean_price * new_midprice,
                product.sell_retail_price / mean_price * new_midprice,
                product.sell_wholesale_price / mean_price * new_midprice
            )
            counter += 1
        return counter

    def update_product_price(self, product, new_buy_price, new_sell_retail_price, new_sell_wholesale_price):
        product.buy_price = new_buy_price
        product.sell_retail_price = new_sell_retail_price
        product.sell_wholesale_price = new_sell_wholesale_price
        product.save()
        self.notify_product_change(product)
        return product

    def notify_product_change(self, product):
        channel_layer = get_channel_layer()
        async_to_sync(channel_layer.group_send)(
            f'product_{product.id}',
            {'type': 'notification', 'data': ProductSerializerWithCategory(product).data}
        )
