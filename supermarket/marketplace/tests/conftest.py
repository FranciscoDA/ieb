import pytest
from ..models import Category, Product


@pytest.fixture
def category():
    category = Category.objects.create(name='Bebidas')
    return Category.objects.get(id=category.id)


@pytest.fixture
def product(category):
    product = Product.objects.create(
        code='000000',
        name='Coca-Cola 1.5L',
        category=category,
        buy_price='100.00',
        sell_retail_price='130.00',
        sell_wholesale_price='120.00',
    )
    return Product.objects.get(id=product.id)
