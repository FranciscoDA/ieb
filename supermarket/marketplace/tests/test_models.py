from decimal import Decimal as D

import pytest

from ..models import Category, Product
from django.db.utils import IntegrityError


@pytest.mark.django_db(transaction=True)
def test_category_names_are_unique():
    a = Category.objects.create(name='a')
    b = Category.objects.create(name='b')

    with pytest.raises(IntegrityError):
        Category.objects.create(name='a')

    assert Category.objects.all().count() == 2


@pytest.mark.django_db(transaction=True)
def test_product_codes_are_unique(category):
    a = Product.objects.create(
        code='0',
        name='a',
        category=category,
        buy_price=D('100.00'),
        sell_retail_price=D('130.00'),
        sell_wholesale_price=D('120.00'),
    )
    b = Product.objects.create(
        code='1',
        name='b',
        category=category,
        buy_price=D('100.00'),
        sell_retail_price=D('130.00'),
        sell_wholesale_price=D('120.00'),
    )

    with pytest.raises(IntegrityError):
        Product.objects.create(
            code='0',
            name='c',
            category=category,
            buy_price=D('100.00'),
            sell_retail_price=D('130.00'),
            sell_wholesale_price=D('120.00'),
        )

    assert Product.objects.all().count() == 2
