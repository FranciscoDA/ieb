import pytest
from ..services import PriceUpdaterService


@pytest.mark.django_db()
def test_price_updater_service(product):
    old_buy_price = product.buy_price
    old_sell_retail_price = product.sell_retail_price
    old_sell_wholesale_price = product.sell_wholesale_price

    update_count = PriceUpdaterService().update_all_product_prices()

    product.refresh_from_db()
    assert product.buy_price != old_buy_price
    assert product.sell_retail_price != old_sell_retail_price
    assert product.sell_wholesale_price != old_sell_wholesale_price
    assert update_count == 1
