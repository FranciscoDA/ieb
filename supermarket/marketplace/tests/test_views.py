import pytest
from rest_framework.test import APIClient


@pytest.mark.django_db()
def test_list_categories(category, product):
    client = APIClient()
    response = client.get('/categories/', format='json')
    assert response.json() == [
        {
            'id': category.id,
            'name': category.name,
            'products': [
                {
                    'id': product.id,
                    'code': product.code,
                    'category': category.id,
                    'name': product.name,
                    'created_at': product.created_at.isoformat().replace('+00:00', 'Z'),
                    'updated_at': product.updated_at.isoformat().replace('+00:00', 'Z'),
                    'buy_price': str(product.buy_price),
                    'sell_retail_price': str(product.sell_retail_price),
                    'sell_wholesale_price': str(product.sell_wholesale_price),
                }
            ]
        }
    ]


@pytest.mark.django_db()
def test_list_products(category, product):
    client = APIClient()

    response = client.get('/products/', format='json')
    assert response.json() == [
        {
            'id': product.id,
            'code': product.code,
            'category': {'id': category.id, 'name': category.name},
            'name': product.name,
            'created_at': product.created_at.isoformat().replace('+00:00', 'Z'),
            'updated_at': product.updated_at.isoformat().replace('+00:00', 'Z'),
            'buy_price': str(product.buy_price),
            'sell_retail_price': str(product.sell_retail_price),
            'sell_wholesale_price': str(product.sell_wholesale_price),
        }
    ]
