import signal
import time
from django.core.management.base import BaseCommand, CommandError
from ...services import PriceUpdaterService


class Command(BaseCommand):
    help = 'Updates prices for all products in this supermarket'

    def add_arguments(self, parser):
        parser.add_argument('update_interval', type=int, help='update interval in seconds')

        self.exit = False
        signal.signal(signal.SIGINT, self._handle_signal)
        signal.signal(signal.SIGTERM, self._handle_signal)

    def _handle_signal(self, signal, frame):
        self.exit = True

    def handle(self, *args, update_interval, **kwargs):
        if update_interval < 0:
            raise CommandError('update_interval cannot be less than 0 seconds')

        price_updater_service = PriceUpdaterService()
        while not self.exit:
            updated = price_updater_service.update_all_product_prices()
            self.stdout.write(self.style.SUCCESS(f'{updated} products have been updated'))
            time.sleep(update_interval)
