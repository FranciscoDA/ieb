from rest_framework import viewsets

from .models import Category, Product
from .serializers import (
    CategorySerializerWithProducts,
    ProductSerializer,
    ProductSerializerWithCategory,
)


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializerWithProducts


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()

    def get_serializer_class(self):
        if self.action in ('list', 'retrieve'):
            return ProductSerializerWithCategory
        return ProductSerializer
