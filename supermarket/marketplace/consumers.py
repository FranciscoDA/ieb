from asgiref.sync import async_to_sync
from channels.generic.websocket import JsonWebsocketConsumer

from .models import Product


class ProductPricesConsumer(JsonWebsocketConsumer):
    groups = ['product_prices']

    def connect(self):
        self.product_id = self.scope['url_route']['kwargs']['product_id']
        self.product_group_name = f'product_{self.product_id}'

        if Product.objects.filter(id=self.product_id).exists():
            async_to_sync(self.channel_layer.group_add)(
                self.product_group_name,
                self.channel_name,
            )
            self.accept()
        else:
            self.close()

    #def receive_json(self, content):
    #    pass

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)(
            self.product_group_name,
            self.channel_name,
        )

    def notification(self, event):
        "Handle product change notification by sending the new data to the client"
        data = event['data']
        self.send_json(data)
