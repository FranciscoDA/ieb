from .models import Product, Category
from rest_framework import serializers


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('name', 'id')


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = (
            'id',
            'code',
            'category',
            'name',

            'created_at',
            'updated_at',

            'buy_price',
            'sell_retail_price',
            'sell_wholesale_price',
        )


class CategorySerializerWithProducts(CategorySerializer):
    products = ProductSerializer(many=True, read_only=True)

    class Meta:
        model = CategorySerializer.Meta.model
        fields = (*CategorySerializer.Meta.fields, 'products')


class ProductSerializerWithCategory(ProductSerializer):
    category = CategorySerializer(read_only=True)
