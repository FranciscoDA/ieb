from django.urls import re_path

from .consumers import ProductPricesConsumer

websocket_urlpatterns = [
    re_path(r'ws/product/(?P<product_id>[a-zA-Z0-9]+)/$', ProductPricesConsumer.as_asgi()),
]
