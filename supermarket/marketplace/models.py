from django.db import models


class Category(models.Model):
    name = models.CharField(unique=True, blank=False, null=False, max_length=255)


class Product(models.Model):
    code = models.CharField(unique=True, blank=False, null=False, max_length=255)
    name = models.CharField(blank=False, null=False, max_length=255)
    category = models.ForeignKey(Category, null=False, on_delete=models.CASCADE, related_name='products')

    created_at = models.DateTimeField(auto_now_add=True, null=False)
    updated_at = models.DateTimeField(auto_now=True, null=False)

    buy_price = models.DecimalField(max_digits=32, decimal_places=16, null=False, blank=False)
    sell_retail_price = models.DecimalField(max_digits=32, decimal_places=16, null=False, blank=False)
    sell_wholesale_price = models.DecimalField(max_digits=32, decimal_places=16, null=False, blank=False)
