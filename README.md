# Candidate information
* Name: Francisco Altoe
* Contact email: francisco@altoe.com.ar

# Project setup and step by step guide

1. Copy the `.env.example` file to `.env`:
```sh
cp .env.example .env
```
2. Start supermarket service
```sh
docker-compose up -d --build supermarket-app
```
3. Add some categories
```sh
curl -H 'Content-Type: application/json' http://127.0.0.1:8000/categories/ -d '{"name": "Bebidas"}'
curl -H 'Content-Type: application/json' http://127.0.0.1:8000/categories/ -d '{"name": "Limpieza"}'
```
4. Add some products
```sh
curl -H 'Content-Type: application/json' -vvv http://127.0.0.1:8000/products/ -d '{"name": "Coca-Cola 1.5L", "code": "001", "buy_price": "100.00", "sell_retail_price": "120.00", "sell_wholesale_price": "110.00", "category": 1}'
curl -H 'Content-Type: application/json' -vvv http://127.0.0.1:8000/products/ -d '{"name": "Lavandina 1L", "code": "002", "buy_price": "100.00", "sell_retail_price": "120.00", "sell_wholesale_price": "110.00", "category": 2}'
```
5. Start the price daemon to update prices every 10 seconds:
```sh
docker-compose up -d --build price-daemon
```
6. Start the client service to connect to the event streams of each product and consume price change notifications. NOTE: if the service was started before creating the products, it must be restarted for it to subscribe to the new event streams.
```sh
docker-compose up -d --build client-app
```
7. Check the `client-app` service logs to be sure alteast some events were received:
```sh
docker-compose logs client-app
```
8. Stop the `client-app` service:
```sh
docker-compose stop client-app
```
9. Check prices output in the CSV files:
```sh
less client/data/supermarket-app\:8000___001.csv
less client/data/supermarket-app\:8000___002.csv
```

# Services

![Service diagram](https://gitlab.com/FranciscoDA/ieb/-/raw/master/doc/services-diagram.png "Service diagram")
## supermarket-app

This is a Django REST Framework web application which also supports sending websocket events on price changes triggered by the `price-updater` daemon. The automatic API documentation can be accessed by opening http://127.0.0.1:8000 in a web browser.

Relevant environment variables:
* `SECRET_KEY`
* `DEBUG`
* `ALLOWED_HOSTS`
* `DATABASE_URL`
* `REDIS_URL`

## price-updater

This is a Django management command that updates product prices randomly. Price updates will trigger a channel layer group message that the `supermarket-app` will receive and use to notify its listeners through the websocket streams.

## client-app

This is a Python application that collects price updates and saves the price histories in CSV files when closed.

On startup, the program queries the categories and products over a set of `supermarket-app` hosts to subscribe to its websocket streams. If there are too many products, the program will chose which products to subscribe to at random. Before data collection begins for any product, the previous price history is loaded from the CSV file.

Relevant environment variables:
* `SUBSCRIPTION_LIMIT` how many event streams to subscribe to (at most).
* `MARKETPLACE_URLS` comma-separated list of hosts that can be queried for products

