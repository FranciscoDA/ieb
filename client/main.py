#! /usr/bin/env python

import sys
import signal
import asyncio
import random

from supermarket_client import SupermarketHttpClient
from core import settings
from product_history import ProductHistory


class Main:
    def __init__(self):
        self.histories: list[ProductHistory] = []
        self._stream_coros = []
        self._main_coro = None
        self._exit_code = 0

    def exit_signal_handler(self, sig, frame):
        "Handle process signals"
        if self._main_coro is not None:
            if sig in (signal.SIGKILL,):
                self._exit_code = 1
            self._main_coro.cancel()

    def prepare_subscriptions(self):
        "Initializes objects and randomly chooses what products to subscribe to"
        products_population = []
        for marketplace_host in settings.MARKETPLACE_URLS:
            client = SupermarketHttpClient(marketplace_host)
            for category in client.get_categories():
                for product in category.products:
                    products_population.append(
                        (marketplace_host, client, category, product)
                    )
        products_sample = random.sample(
            products_population,
            k=min(len(products_population), settings.SUBSCRIPTION_LIMIT)
        )
        for marketplace_host, client, category, product in products_sample:
            product_history = ProductHistory('./data/', marketplace_host, category, product)
            product_history.load()
            self.histories.append(product_history)
            self._stream_coros.append(self.fill_product_history(client, product_history))

    async def fill_product_history(self, client, product_history):
        "Fill product price history with data coming from the event stream"

        async for product_update in client.product_event_stream(product_history.product.id):
            product_history.add_price(
                product_update.updated_at,
                product_update.buy_price,
                product_update.sell_retail_price,
                product_update.sell_wholesale_price
            )

    async def listen_subscriptions(self):
        "Coroutine that listens events on the proposed websocket endpoints"
        self._main_coro = asyncio.gather(*self._stream_coros)
        await self._main_coro

    def post_exit(self):
        "Execute tasks before exiting this process"
        print('Saving data...')
        for product_history in self.histories:
            product_history.save()
        print('Data saved')

    def main(self):
        self.prepare_subscriptions()
        signal.signal(signal.SIGINT, self.exit_signal_handler)
        signal.signal(signal.SIGTERM, self.exit_signal_handler)
        try:
            asyncio.run(self.listen_subscriptions())
        except asyncio.exceptions.CancelledError:
            self.post_exit()
        sys.exit(self._exit_code)


if __name__ == '__main__':
    Main().main()
