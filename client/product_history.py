from decimal import Decimal
from datetime import datetime
import pathlib
import pandas as pd
from supermarket_client import Category, Product


class ProductHistory:
    "Represents a product and its price variations during a period"

    def __init__(self, datadir: str, marketplace: str, category: Category, product: Product):
        self.datadir = datadir
        self.marketplace = marketplace
        self.category = category
        self.product = product
        self.timestamps = []
        self.buy_prices = []
        self.sell_retail_prices = []
        self.sell_wholesale_prices = []

    def add_price(self, timestamp: datetime, buy_price: Decimal, sell_retail_price: Decimal, sell_wholesale_price: Decimal):
        "Adds a new \"row\" into the price time series"
        self.timestamps.append(timestamp)
        self.buy_prices.append(buy_price)
        self.sell_retail_prices.append(sell_retail_price)
        self.sell_wholesale_prices.append(sell_wholesale_price)

    def as_dataframe(self, include_metadata=False) -> pd.DataFrame:
        "Converts the time series to a pandas dataframe"
        columns = {
            'timestamp': self.timestamps,
            'buy_price': self.buy_prices,
            'sell_retail_price': self.sell_retail_prices,
            'sell_wholesale_price': self.sell_wholesale_prices,
        }
        if include_metadata:
            columns = {
                'marketplace': self.marketplace,
                'category_name': self.category.name,
                'product_code': self.product.code,
                'product_name': self.product.name,
                **columns
            }
        return pd.DataFrame(columns)

    def _file_name(self):
        clean_marketplace_host = self.marketplace.removeprefix('https://').removeprefix('http://').replace('/', '__')
        datadir_path = pathlib.Path(self.datadir)
        return datadir_path / f'{clean_marketplace_host}_{self.product.code}.csv'

    def save(self):
        self.as_dataframe().to_csv(self._file_name())

    def load(self):
        try:
            df = pd.read_csv(
                self._file_name(),
                converters={
                    'buy_price': Decimal,
                    'sell_retail_price': Decimal,
                    'sell_wholesale_price': Decimal,
                },
                parse_dates=['timestamp'],
            )
        except FileNotFoundError:
            return
        self.timestamps = list(df['timestamp'])
        self.buy_prices = list(df['buy_price'])
        self.sell_retail_prices = list(df['sell_retail_price'])
        self.sell_wholesale_prices = list(df['sell_wholesale_price'])
