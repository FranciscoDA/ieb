from environs import Env

env = Env()


DEBUG = env.bool('DEBUG', default=False)
SUBSCRIPTION_LIMIT = env.int('SUBSCRIPTION_LIMIT', default=10)
MARKETPLACE_URLS = env.list('MARKETPLACE_URLS', required=True)
