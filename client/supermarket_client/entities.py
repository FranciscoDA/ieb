import dataclasses
from datetime import datetime
from decimal import Decimal


def _cast_fields(obj):
    "Converts fields from a seriailzed format into the expected types"
    for field in dataclasses.fields(obj):
        value = getattr(obj, field.name)
        if field.type == datetime and isinstance(value, str):
            for suf in ('+0000', 'Z'):
                if value.endswith(suf):
                    value = datetime.fromisoformat(value.removesuffix(suf) + '+00:00')
            setattr(obj, field.name, value)
        elif field.type == Decimal and isinstance(value, str):
            setattr(obj, field.name, Decimal(value))


@dataclasses.dataclass
class Product:
    id: int
    code: str
    category: int
    name: str

    created_at: datetime
    updated_at: datetime

    buy_price: Decimal
    sell_retail_price: Decimal
    sell_wholesale_price: Decimal

    def __post_init__(self):
        _cast_fields(self)


@dataclasses.dataclass
class Category:
    id: int
    name: str
    products: list[Product] = dataclasses.field(default_factory=list, hash=False)

    def __post_init__(self):
        _cast_fields(self)
