from urllib.parse import urljoin, urlparse, urlunparse
import json

from urllib3.util.retry import Retry

import websockets
import requests

from .entities import Product, Category


class SupermarketHttpClient:
    def __init__(self, url):
        self.url_prefix = url
        if self.url_prefix and self.url_prefix[-1] != '/':
            self.url_prefix += '/'

        self.session = requests.Session()
        self.session.mount('http://', requests.adapters.HTTPAdapter(max_retries=Retry(
            total=10, backoff_factor=0.5,
        )))

    def _build_endpoint_url(self, path):
        return urljoin(self.url_prefix, path)

    def _parse_category(self, category_json):
        products_json = category_json.pop('products')
        return Category(
            **category_json,
            products=[Product(**product_json) for product_json in products_json]
        )

    def get_categories(self):
        "Gets all categories from the endpoint in the supermarket service"
        response = self.session.get(self._build_endpoint_url('categories/'))
        response.raise_for_status()
        categories_json = response.json()
        categories = [
            self._parse_category(category_json)
            for category_json in categories_json
        ]
        return categories

    def get_category(self, category_id):
        "Gets a single category from the endpoint in the supermarket service"
        response = self.session.get(self._build_endpoint_url(f'categories/{category_id}/'))
        response.raise_for_status()
        return self._parse_category(response.json())

    def _parse_product(self, product_json):
        category_json = product_json.pop('category')
        return Product(
            **product_json,
            category=Category(**category_json)
        )

    def get_products(self):
        "Gets all products from the endpoint in the supermarket service"
        response = self.session.get(self._build_endpoint_url('products/'))
        response.raise_for_status()
        products_json = response.json()
        return [
            self._parse_product(product_json)
            for product_json in products_json
        ]

    def get_product(self, product_id):
        "Gets a single product from the endpoint in the supermarket service"
        response = self.session.get(self._build_endpoint_url(f'products/{product_id}/'))
        response.raise_for_status()
        return self._parse_product(response.json())

    async def product_event_stream(self, product_id):
        "Subscribes to websocket event stream for a product and yields updated prices"

        # transform protocol scheme to websocket or secure websockets
        parse_result = urlparse(self.url_prefix)
        if parse_result.scheme == 'http':
            transformed_url = parse_result._replace(scheme='ws')
        elif parse_result.scheme == 'https':
            transformed_url = parse_result._replace(scheme='wss')
        transformed_url = urlunparse(transformed_url)

        event_stream_url = urljoin(transformed_url, f'ws/product/{product_id}/')

        async for websocket in websockets.connect(event_stream_url):
            try:
                while True:
                    product_data = await websocket.recv()
                    print('got data:', product_data)
                    product_json = json.loads(product_data)
                    product = self._parse_product(product_json)
                    yield product
            except websockets.ConnectionClosed:
                print(f'Websocket closed connection to {event_stream_url}, retrying...')
                continue
